const _ = require('lodash')
const path = require('path')
const { writeFileSync } = require('fs')

const deepclean = require('clean-deep')
const deepsort = require('deep-sort-object')
const deepmerge = require('../src/utils/deepmerge.js')

const { camelCase,
        extractMethodNamesForScopeFromMethodList,
        extractNamespaceFromURL,
        extractScopesFromMethodsList,
        getDuplicates,
        pascalCase} = require('./helpers.js')

const API_NAMES = require('../src/routes/api-names.json')
const PATHS_SPEC = require('../specification/paths.json')

const PATHS_SPEC_EXTRAS = require('../specification/extras/paths.json')

const routesPath = path.resolve('src/routes/routes.json')

const routesObject = {}

const initializeRoutes = routesObject => {
  const namespaces = extractScopesFromMethodsList(API_NAMES)

  _.each(namespaces, namespaceName => {
    // Initialize Namespace
    routesObject[namespaceName] = {}

    let methodNames = extractMethodNamesForScopeFromMethodList(
      API_NAMES,
      namespaceName
    )

    // Check for duplicate MethodNames
    let duplicates = getDuplicates(methodNames)
    if (duplicates.length) {
      console.log(`Duplicate MethodNames:[${duplicates}] in Scope:[${namespaceName}]`)
      console.log(`Ignoring the duplicate!`)

    }

    // Initialize MethodNames
    _.each(methodNames, methodName => {
      methodName = camelCase(methodName)
      routesObject[namespaceName][methodName] = {
        params: {}
      }
    })
  })
}

const setAlias = (apiObject, namespaceName, resourceName, namesList) => {
  if (namespaceName !== resourceName && namesList[resourceName]) {
    apiObject.alias = `${resourceName}.${namesList[resourceName]}`
    return true
  }

  return false
}

const setBodyType = (apiObject, { requestBody }) => {
  if (!requestBody) {
    return
  }

  let bodyType

  if (requestBody.$ref) {
    bodyType = requestBody.$ref
  } else if (requestBody.content) {
    if (
      !requestBody.content['application/json'] ||
      !requestBody.content['application/json'].schema
    ) {
      return
    }

    bodyType = requestBody.content['application/json'].schema.$ref
  }

  if (!bodyType) {
    return
  }

  apiObject.bodyType = pascalCase(bodyType.replace(/#\/components\/\w+?\//, ''))
}

const setHTTPMethod = (apiObject, method) => {
  apiObject.method = method.toUpperCase()
}

const setParameters = (apiObject, { parameters = [] }) => {
  
  _.each(
    parameters,
    ({in: _in, name,required, schema: {enum: _enum, items, type = 'any' } }) => {
      
      if (!apiObject.params[name]) {
        apiObject.params[name] = {}
      }

      apiObject.params[name] = deepmerge(apiObject.params[name], {
        enum: _enum,
        in: _in,
        type
      })

      if (items && items.type) {
        apiObject.params[name].itemsType = items.type
      } else if (items && items.$ref && type) {
        apiObject.params[name].itemsType = pascalCase(
         items.$ref.replace('#/components/schemas/', '')
        )

        if (items.type === "array") {
          apiObject.params[name].itemsType = apiObject.params[name].itemsType =  + "[]"
        }
      }

      if (required) {
        apiObject.params[name].required = required
      }

      apiObject.params[name].enum = _.uniq(apiObject.params[name].enum)

      if (type === 'any' && schema.$ref) {
        apiObject.params[name].schema = pascalCase(
          schema.$ref.replace('#/components/schemas/', '')
        )
      }

    }
  )
}

const responseType$ref = (response) => {
  if(!response.content || !response.content['application/json'] || !response.content['application/json'].schema){ 
    return undefined }
  
  if(response.content['application/json'].schema.$ref) {
    return { ref:response.content['application/json'].schema.$ref, isArray:false };
  }

  if(!response.content['application/json'].schema.items) {
      return undefined;
  } 
    
  if(response.content['application/json'].schema.items.$ref) { 
    return {ref: response.content['application/json'].schema.items.$ref, isArray:true };
  }

  return undefined;
}
const setReturnTypes = (apiObject, { responses = [] }) => {

  _.each(responses, (response, code) => {
    if (Number(code) < 400) {

      let respRef = responseType$ref(response)
      if (!respRef){ return }
    
      let respType = pascalCase(
        respRef.ref.replace(
          /#\/components\/\w+.*?\//,
          ''
        )
      );
      
      if (respRef.isArray) {
        respType = respType + '[]';
      }
      apiObject.responseType = respType;
    }
  })
}

const setURL = (apiObject, url) => {
  apiObject.url = url
}

const updateRoutes = routesObject => {
  _.each(API_NAMES, (methods, url) => {
    // Specification for URL
    let spec = PATHS_SPEC[url] || {}
    let specExtras = PATHS_SPEC_EXTRAS[url] || {}

    _.each(methods, (namespaces, method) => {
      _.each(namespaces, (apiName, namespaceName) => {
        apiName = camelCase(apiName)
        // Igonre Empty MethodNames
        if (!apiName) return

        if (
          setAlias(
            routesObject[namespaceName][apiName],
            namespaceName,
            extractNamespaceFromURL(url),
            API_NAMES[url][method]
          )
        ) {
          return
        }

        setHTTPMethod(routesObject[namespaceName][apiName], method)
        
        setURL(routesObject[namespaceName][apiName], url)

        setParameters(routesObject[namespaceName][apiName], spec)

        
        
        if (spec[method]) {
          setBodyType(routesObject[namespaceName][apiName], spec[method])
          setParameters(routesObject[namespaceName][apiName], spec[method])
          
          setReturnTypes(routesObject[namespaceName][apiName], spec[method])
        }

        if (specExtras[method]) {
          setBodyType(routesObject[namespaceName][apiName], specExtras[method])
          setParameters(routesObject[namespaceName][apiName], specExtras[method])
          setReturnTypes(routesObject[namespaceName][apiName], specExtras[method])
        }
      })
    })
  })
}

initializeRoutes(routesObject)
updateRoutes(routesObject)

writeFileSync(
  routesPath,
  `${JSON.stringify(deepsort(deepclean(routesObject)), null, 2)}\n`
)
