const path = require('path')
const { writeFileSync } = require('fs')

const srcPath = path.resolve('specification')

 writeFileSync(
    path.resolve(srcPath, `paths.json`),
    `{\n}\n`
  )

  writeFileSync(
    path.resolve(srcPath, `requestBodies.json`),
    `{\n}\n`
  )

  writeFileSync(
    path.resolve(srcPath, `schemas.json`),
    `{\n}\n`
  )

  writeFileSync(
    path.resolve(srcPath, `securitySchemes.json`),
    `{\n}\n`
  )

  writeFileSync(
    path.resolve(srcPath, `others.json`),
    `{\n}\n`
  )
